# ads-c

> **A**lgorithms and **D**ata **S**tructures in C

[Low Level Learning][links.yt.low-level-learning] demonstrated how to code up a singly-linked list in C in their video
[_every good programmer should know how to code this data structrure_][links.yt.low-level-learning.vid]. After following
along, I decided it might be a fun idea to keep a running project for any of these I try in the future, guided or not.

Even though C isn't something I use much these days, doing these exercises with it drives me to mind my data structures a
bit more than some of the more modern technologies I work with might. Also, the compiler won't let me get away with much.

## Usage

Each file in the [src/][./src] directory is made to stand on its own, so they can each be compiled and executed
separately.

Run the [`Makefile`](./Makefile) with `make` to compile everything. This is also done upon loading the project's dev container.

## Compiling

## Development

This project is equipped with a [dev container][links.devcontainers] config that runs
[Ubuntu 22.04.3 LTS (Jammy Jellfyfish)][links.ubuntu-rel-jammy]. It should also run out
of the box on most modern Linux distributions, as its only true dependencies are a C compiler like [GCC][links.gcc] and
[Bash][links.bash] (though `sh` will work). [`Make`][links.make] is also nice, but not required.

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC
BY-SA 4.0</sub>

<hr>

<!-- Links -->

[links.yt.low-level-learning]: https://www.youtube.com/@LowLevelLearning
[links.yt.low-level-learning.vid]: https://www.youtube.com/watch?v=dti0F7w3yOQ
[links.ubuntu-rel-jammy]: https://releases.ubuntu.com/jammy/
[links.devcontainers]: https://containers.dev/
[links.gcc]: https://gcc.gnu.org/
[links.bash]: https://www.gnu.org/software/bash/
[links.make]: https://www.gnu.org/software/make/
