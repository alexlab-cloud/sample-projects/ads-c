#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


/**
 * Singly-linked List
 * ------------------
 * This was demonstrated in a video by Low Level Learning here: https://www.youtube.com/@LowLevelLearning.
 * I made some adjustments based on pull requests to the sample project by the community to ensure memory was
 * freed everywhere appropriately.
 *
 * If head == NULL, then the list is empty. Start by allocating a node and then pointing that node
 * to the head.
 *
 * When adding nodes, adding them to the end has a time complexity of O(n) because we have to traverse
 * the entire list to find the end. Instead, allocate a new node, point that node to the same node pointed
 * to by the head, and then reset the head to point to our newest node. This is a O(1) "constant time" operation.
*/


typedef struct {
    void *next;
    int data;
} Node;


// Initialize the head of the singly-linked list with a NULL head node
Node *head = NULL;


/**
 * Add a node to the list
*/
Node *addNode(int data) {
    Node *new = NULL;

    if (head == NULL) {
        // List is empty
        new = malloc(sizeof(Node));
        if (new == NULL) {
            return NULL;
        }

        new->data = data;
        head = new;
        new->next = NULL;

        return new;
    }

    // Default
    new = malloc(sizeof(Node));
    if (new == NULL) {
        return NULL;
    }

    new->data = data;
    new->next = head;
    head = new;

    return new;
}


/**
 * Remove a node from the list
*/
bool remNode(int data) {
    Node *current = head;
    Node *prev = head;

    while (current != NULL) {
        if (current->data == data) {
            if (current == head) {
                // If the current node is the list head, then
                Node *new_head = head->next;
                free(head);
                head = new_head;
            }

            prev->next = current->next;
            free(current);

            return true;
        }

        prev = current;
        current = current->next;
    }

    // Default
    return false;
}


/**
 * Insert a node into the list
*/
Node *insNode(int data, int position) {
    Node *current = head;

    while (current != NULL && --position != 0) {
        current = current->next;
    }

    if (position != 0) {
        printf("Requested position too far into list\n");
        return NULL;
    }

    Node *new = malloc(sizeof(Node));
    if (new == NULL) {
        return NULL;
    }

    new->data = data;
    new->next = current->next;
    current->next = new;

    // Return the newly inserted node
    return new;
}


/**
 * Display the list
*/
void dispList() {
    // Set
    Node *current = head;

    // Print the list elements until we're back at the original head node
    while (current != NULL) {
        printf("%d->", current->data);
        current = current->next;
    }

    printf("\n");
    return;
}


void printMenu() {
    printf("\nOPTIONS:\n");
    printf("\t1. ADD a node to the list\n");
    printf("\t2. REMOVE a node from the list\n");
    printf("\t3. INSERT a node to the list\n");
    printf("\t4. DISPLAY the list\n");
    printf("\t5. QUIT\n");
    printf("> ");
    return;
}


/**
 * Spin up a simple text-based UI to interact with a singly-linked list using the methods defined above.
*/
int main(int argc, char **argv) {
    // Initialize the variables that drive the UI
    int menuOpt = -1;
    int arg1 = 0;
    int arg2 = 0;

    while (menuOpt != 5) {
        printMenu();
        int numOptsReceived = scanf("%d", &menuOpt);

        if (numOptsReceived == 1 && menuOpt > 0 && menuOpt < 5) {
            printf("\n");

            switch(menuOpt) {
                case 1:
                    printf("-- ADD NODE --\n");
                    printf("New node data:\n");
                    printf("> ");
                    scanf("%d", &arg1);

                    addNode(arg1);
                    break;
                case 2:
                    printf("-- DELETE NODE --\n");
                    printf("Remove which node?\n");
                    printf("> ");
                    scanf("%d", &arg1);

                    bool success = remNode(arg1);
                    if (!success) {
                        printf("Element not found\n");
                    }

                    break;
                case 3:
                    printf("-- INSERT NODE --\n");
                    printf("Integer data to insert: \n");
                    printf("> ");
                    scanf("%d", &arg1);
                    printf("Position to insert new node:\n");
                    printf("> ");
                    scanf("%d", &arg2);

                    Node *new = insNode(arg1, arg2);
                    if (new == NULL) {
                        printf("Failed to insert new node into list\n");
                    }

                    break;
                case 4:
                    printf("-- CURRENT LIST --\n");
                    dispList();
                    break;
                case 5:
                    break;
            }
        }
    }

    // Memory cleanup
    while (head != NULL) {
        Node *new_head = head->next;
        free(head);
        head = new_head;
    }

    return 0;
}
