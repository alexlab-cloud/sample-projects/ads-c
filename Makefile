# Created with lots of help from from these answers: https://stackoverflow.com/a/31192185
# - https://stackoverflow.com/a/2145605/6683107

SRC := src
BIN := bin

CFLAGS=-Wall -g

SOURCES := $(wildcard $(SRC)/*.c)
BINARIES=$(SOURCES:$(SRC)/%.c=%)

all: $(BINARIES)

%: $(SRC)/%.c
	$(CC) $(CFLAGS) -o $(BIN)/$@ $<

.PHONY: clean

clean:
	rm -f $(BINARIES)
